module bidir_io(
  input dir,
  input d,
  inout port);
  assign port = (dir) ? 1'bz : d;
endmodule

module top(
   // Outputs
   // s,          // Onboard LED
   RS232_TXD,  // RS232 transmit
   RESET_TRIGGER,      // RESET-TRIGGER#
 
   // Inputs
   clka,
 
   pb_a, pb_d, pb_rd_n, pb_wr_n,
 
   ether_cs_n, ether_aen, ether_bhe_n, ether_clk, ether_irq, ether_rdy,
 
   // Flash
   flash_a, flash_d, 
   flash_ce_n, flash_oe_n, flash_we_n, flash_byte_n, flash_rdy, flash_rst_n,
 
   // PS/2 Keyboard
   ps2_clk, ps2_dat,
 
   // Pushbuttons
   sw2_n, sw3_n
 
 //   // VGA
 //   vga_red, vga_green, vga_blue, vga_hsync_n, vga_vsync_n,
 
   );
  
  // output [7:0] s;
  output RS232_TXD;
  output RESET_TRIGGER;
  inout [4:0] pb_a;
  output ether_cs_n;
  output ether_aen;
  output ether_bhe_n;
  output pb_rd_n;
  output pb_wr_n;

  input clka;
  input ether_clk;
  input ether_irq;
  input ether_rdy;

  inout [15:0] pb_d;

  output [19:0] flash_a;

  inout [15:0] flash_d;

  output flash_ce_n;
  output flash_oe_n;
  output flash_we_n;
  output flash_byte_n;
  output flash_rdy;
  output flash_rst_n;

  reg ps2_clk_dir;
  reg ps2_dat_dir;
  reg ps2_clk_d;
  reg ps2_dat_d;
  inout ps2_clk;
  inout ps2_dat;
  bidir_io ps2_clkb(.dir(ps2_clk_dir), .d(ps2_clk_d), .port(ps2_clk));
  bidir_io ps2_datb(.dir(ps2_dat_dir), .d(ps2_dat_d), .port(ps2_dat));

  input sw2_n;
  input sw3_n;

  wire j1_io_rd;
  wire j1_io_wr;
  wire [15:0] j1_io_addr;
  reg  [15:0] j1_io_din;
  wire [15:0] j1_io_dout;

  wire sys_clk;
  ck_div #(.DIV_BY(12), .MULT_BY(4)) sys_ck_gen(.ck_in(clka), .ck_out(sys_clk));

  // ================================================
  // Hardware multiplier

  reg [15:0] mult_a;
  reg [15:0] mult_b;
  wire [31:0] mult_p;
  wire[3:0] ignore;
  MULT18X18 mulinsn(.A({1'b0, 1'b0, mult_a}), .B({1'b0, 1'b0, mult_b}), .P({ignore, mult_p}));
//   MULT18X18SIO #(
//     .AREG(0),
//     .BREG(0),
//     .PREG(0))
//   MULT18X18SIO(
//     .A(mult_a),
//     .B(mult_b),
//     .P(mult_p));

  // ================================================
  // 32-bit 1-MHz system clock

  reg  [5:0]  clockus;
  wire [5:0]  _clockus = (clockus == 32) ? 0 : (clockus + 1);
  reg  [31:0] clock;
  wire [31:0] _clock = (clockus == 32) ? (clock + 1) : (clock);

  always @(posedge sys_clk)
  begin
    clockus <= _clockus;
    clock <= _clock;
  end

  // reg [7:0] s;
  reg RS232_TXD;
  reg RESET_TRIGGER;

  reg ether_cs_n;
  reg ether_aen;
  reg ether_bhe_n;
  reg ddir;

  reg [15:0] pb_dout;
  assign pb_d = (ddir) ? 16'bz : pb_dout;
  reg pb_rd_n;
  reg pb_wr_n;

  reg pb_a_dir;
  reg [4:0] pb_aout;
  assign pb_a = pb_a_dir ? 5'bz : pb_aout;

  reg flash_ddir;
  reg [19:0] flash_a;
  reg [15:0] flash_dout;
  assign flash_d[14:0] = (flash_ddir) ? 15'bz : flash_dout[14:0];
  assign flash_d[15] = (flash_ddir & flash_byte_n) ? 1'bz : flash_dout[15];
  reg flash_ce_n;
  reg flash_oe_n;
  reg flash_we_n;
  reg flash_byte_n;
  reg flash_rdy;
  reg flash_rst_n;

  always @(posedge sys_clk)
  begin
    if (j1_io_wr) begin
      case (j1_io_addr)
      // 16'h4000: s <= j1_io_dout;

      16'h4100: flash_ddir <= j1_io_dout;
      16'h4102: flash_ce_n <= j1_io_dout;
      16'h4104: flash_oe_n <= j1_io_dout;
      16'h4106: flash_we_n <= j1_io_dout;
      16'h4108: flash_byte_n <= j1_io_dout;
      16'h410a: flash_rdy <= j1_io_dout;
      16'h410c: flash_rst_n <= j1_io_dout;
      16'h410e: flash_a[15:0] <= j1_io_dout;
      16'h4110: flash_a[19:16] <= j1_io_dout;
      16'h4112: flash_dout <= j1_io_dout;

      16'h4200: ps2_clk_d <= j1_io_dout;
      16'h4202: ps2_dat_d <= j1_io_dout;
      16'h4204: ps2_clk_dir <= j1_io_dout;
      16'h4206: ps2_dat_dir <= j1_io_dout;

      16'h5000: RS232_TXD <= j1_io_dout;
      16'h5001: RESET_TRIGGER <= j1_io_dout;
      16'h5100: ether_cs_n <= j1_io_dout;
      16'h5101: ether_aen <= j1_io_dout;
      16'h5102: ether_bhe_n <= j1_io_dout;
      16'h5103: pb_aout <= j1_io_dout;
      16'h5104: ddir <= j1_io_dout;
      16'h5105: pb_dout <= j1_io_dout;
      16'h5106: pb_rd_n <= j1_io_dout;
      16'h5107: pb_wr_n <= j1_io_dout;
      // 5108
      // 5109
      16'h510a: pb_a_dir <= j1_io_dout;

      16'h6100: mult_a <= j1_io_dout;
      16'h6102: mult_b <= j1_io_dout;

      endcase
    end
  end

  always @*
  begin
    case (j1_io_addr)
    16'h4112: j1_io_din = flash_d;

    16'h4200: j1_io_din = ps2_clk;
    16'h4202: j1_io_din = ps2_dat;

    16'h4500: j1_io_din = sw2_n;
    16'h4502: j1_io_din = sw3_n;

    16'h5103: j1_io_din = pb_a;
    16'h5105: j1_io_din = pb_d;
    16'h5108: j1_io_din = ether_rdy;
    16'h5109: j1_io_din = ether_irq;

    16'h6000: j1_io_din = clock[15:0];
    16'h6002: j1_io_din = clock[31:16];

    16'h6104: j1_io_din = mult_p[15:0];
    16'h6106: j1_io_din = mult_p[31:16];

    default: j1_io_din = 16'h0946;
    endcase
  end

  reg [10:0] reset_count = 1000;
  wire sys_rst_i = |reset_count;
   
  always @(posedge sys_clk) begin
    if (sys_rst_i)
      reset_count <= reset_count - 1;
  end 

  j1 j1(
       // Inputs
       .sys_clk_i                 (sys_clk),
       .sys_rst_i                 (sys_rst_i),

       .io_rd(j1_io_rd),
       .io_wr(j1_io_wr),
       .io_addr(j1_io_addr),
       .io_din(j1_io_din),
       .io_dout(j1_io_dout)
       );

  /*
  uart uart(
            // Outputs
            .uart_busy                 (uart_busy),
            .uart_tx                   (RS232_TXD),
            // Inputs
            .uart_wr_i                 (j1_uart_we),
            .uart_dat_i                (j1_io_dout),
            .sys_clk_i                 (sys_clk_i),
            .sys_rst_i                 (sys_rst_i));
  */

endmodule // top

